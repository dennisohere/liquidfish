## Setup

- Install php dependencies via 'composer install'
- Set environment variables with .env file.
  If file does not exist make copy from .env.example.
- Run database migrations (php artisan migrate)
- Generate app key (php artisan key:generate)
- Launch app via 'php artisan serve' (or from whichever server of your choice)
- Mails are configured to 'log'. You can swap to your preferred mail driver and credentials

### Packages / Dependencies
- Tailwind CSS
- Sweetalert
- JQuery


