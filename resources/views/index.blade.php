<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Liquid Fish | Contact Us</title>
</head>
<body>

<div class="w-screen md:flex">
    <div id="contact-info" class="p-10 md:w-1/4 sm:w-full bg-gradient-to-b from-indigo-700 via-indigo-700 to-indigo-600 md:h-screen ">

        <h5 class="text-white text-lg mb-6 font-medium">Contact Information</h5>

        <p class="text-white text-sm font-light mb-4">
            <i class="fa fa-phone text-lg"></i>
            <span class="ml-3">+1 (555) 123-4567</span>
        </p>

        <p class="text-white text-sm font-light mb-4">
            <i class="fa fa-envelope text-lg"></i>
            <span class="ml-3">support@liquidfish.com</span>
        </p>

        <p class="text-white text-sm font-light flex space-x-8">
            <a href="">
                <i class="fa fa-facebook-official text-2xl"></i>
            </a>
            <a href="">
                <i class="fa fa-github text-2xl"></i>
            </a>
            <a href="">
                <i class="fa fa-twitter text-2xl"></i>
            </a>
        </p>
    </div>

    <div class="p-10 sm:w-full md:w-3/4 md:h-screen"
        id="message-form-wrapper">
        <h5 class="text-lg mb-6 font-semibold text-gray-700">Send us a message</h5>

        <form class="w-full"
            id="message-form">
            @csrf

            <div class="md:flex md:justify-between ">
                <div class="md:w-1/2 sm:w-full md:mr-4 mb-5">
                    <div class="w-full">
                        <p class="m-0 mb-1">
                            <span class="font-medium text-gray-700 text-sm">First name</span>
                        </p>
                        <input class="w-full border-2 rounded-md px-2.5 py-1.5 "
                               type="text" name="first_name">
                    </div>
                </div>

                <div class="md:w-1/2 sm:w-full md:ml-4 mb-5">
                    <div class="w-full">
                        <p class="m-0 mb-1">
                            <span class="font-medium text-gray-700 text-sm">Last name</span>
                        </p>
                        <input class="w-full border-2 rounded-md px-2.5 py-1.5 "
                               type="text" name="last_name">
                    </div>
                </div>
            </div>

            <div class="md:flex md:justify-between">
                <div class="md:w-1/2 sm:w-full md:mr-4 mb-5">
                    <div class="w-full">
                        <p class="m-0 mb-1">
                            <span class="font-medium text-gray-700 text-sm">Email</span>
                        </p>
                        <input class="w-full border-2 rounded-md px-2.5 py-1.5 "
                               type="email" name="email">
                    </div>
                </div>

                <div class="md:w-1/2 sm:w-full md:ml-4 mb-5">
                    <div class="w-full">
                        <p class="m-0 mb-1">
                            <span class="font-medium text-gray-700 text-sm">Phone</span>
                            <span class="text-xs text-gray-400 float-right">Optional</span>
                        </p>
                        <input class="w-full border-2 rounded-md px-2.5 py-1.5 "
                               type="text" name="phone">
                    </div>
                </div>
            </div>

            <div class="w-full mb-5">
                <p class="m-0 mb-1">
                    <span class="font-medium text-gray-700 text-sm">Subject</span>
                </p>
                <input class="w-full border-2 rounded-md px-2.5 py-1.5 "
                       type="text" name="subject">
            </div>


            <div class="w-full mb-5">
                <p class="m-0 mb-1">
                    <span class="font-medium text-gray-700 text-sm">Message</span>
                    <span class="text-xs text-gray-400 float-right">Max. 500 Characters</span>
                </p>
                <textarea name="message" rows="5" class="w-full border-2 rounded-md px-2.5 py-1.5"></textarea>
            </div>

            <div class="w-full flex justify-end">
                <button type="submit" class="bg-indigo-600 text-white px-4 py-2 rounded hover:bg-indigo-800 hover:shadow-2xl">
                    Submit
                </button>
            </div>

        </form>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function(){
        $("#message-form").submit(function (event) {
            event.preventDefault();
            let form = $(this);
            var payload = {},
                formDataArray = form.serializeArray();

            for (var i in formDataArray) {
                payload[formDataArray[i].name] = formDataArray[i].value;
            }

            $.post({
                url: '/',
                data: payload,
                dataType: "json",
            }).then(function(response){
                window.Swal.fire({
                    icon: 'success',
                    title: 'Message sent!',
                    text: 'Thank you for contacting us.',
                }).then(function(){
                    form.trigger('reset');
                });
            }).catch(function (error) {
                console.log('error', error);
                window.Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: error.responseJSON.message,
                    footer: `<p class="text-red-400 text-center">Kindly confirm that you have inputted all required fields.</p>`
                })
            })
        });
    })
</script>

</body>
</html>
