<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Message
     */
    public function store(Request $request): Message
    {
        $payload = $this->validate($request, [
            'first_name' => 'bail|required|string',
            'last_name' => 'bail|required|string',
            'email' => 'bail|required|string|email',
            'phone' => 'bail|nullable|string',
            'subject' => 'bail|required|string',
            'message' => 'bail|required|string|max:500',
        ]);

        $message = new Message();

        $message->fill($payload);

        $message->save();

        event(new MessageSent($message));

        return $message;
    }





}
