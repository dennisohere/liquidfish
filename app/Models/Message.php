<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package App\Models
 *
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property string subject
 * @property string message
 *
 */
class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'subject',
        'message'
    ];
}
