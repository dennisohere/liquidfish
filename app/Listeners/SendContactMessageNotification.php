<?php

namespace App\Listeners;

use App\Events\MessageSent;
use App\Notifications\ContactMessageNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendContactMessageNotification implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var ContactMessageNotification
     */
    private $contactMessageNotification;

    /**
     * Create the event listener.
     *
     * @param ContactMessageNotification $contactMessageNotification
     */
    public function __construct(ContactMessageNotification $contactMessageNotification)
    {
        //
        $this->contactMessageNotification = $contactMessageNotification;
    }

    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $message = $event->message;

        Notification::route('mail', 'dennisohere@live.com')
            ->notify(new ContactMessageNotification($message));
    }
}
